package com.example.ravi.soundequilizers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var supportActionBar = toolbar
        toolbar.setLogo(R.drawable.ic_chevron_left_black_24dp)
        var soundEqulizer : SoundEqulizersAdapter =  SoundEqulizersAdapter(supportFragmentManager)
        tablayout.setupWithViewPager(viewpager)
        viewpager.adapter = soundEqulizer

    }
}
